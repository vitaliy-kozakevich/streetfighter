import Info from './fighterInfo';
import View from './view';
import FighterView from './fighterView';
import { popup } from './modal';

// створюємо клас Арена для відображення бійки
class Arena extends View {
	constructor(options, state) {
		super();
		this.state = state;
		this.overlay = document.querySelector(options.overlay);
		this.fighterInfoClass = new Info();

		this.open = this.open.bind(this);
		this.fightArena = this.fightArena.bind(this);
		this.fight = this.fight.bind(this);
	}

	// відкриття Арени
	open(state) {
		this.overlay.classList.add('open');
		this.fightArena(state);
	}
	fightArena(fighters) {
		const fighterElements = fighters.map(fighter => {
			const fighterView = new FighterView(fighter, () => {});
			return fighterView.element;
		});
		let figter1Btn;
		let figter2Btn;
		let figter1BtnId;
		let figter2BtnId;
		let divFigter1Info;
		let divFigter2Info;
		let divFighterInfo;

		// створюємо клас 'fighters' і поміщаємо туди бійців, розвертаємо 2-го бійця  на 180 градусів
		this.element = this.createElement({
			tagName: 'div',
			className: 'fighters',
		});
		this.element.append(...fighterElements);
		fighterElements[1].setAttribute('class', 'fighter fighter-rotate');
		fighterElements[1].childNodes[1].setAttribute('class', 'name back-rotate');
		for (let i = 0; i < this.state.length; i++) {
			divFighterInfo = this.fighterInfoClass.createFighterInfo(this.state[i]);
			fighterElements[i].append(divFighterInfo);
		}
		divFigter1Info = fighterElements[0].childNodes[2];
		divFigter2Info = fighterElements[1].childNodes[2];
		figter1Btn = fighterElements[0].childNodes[2].childNodes[6];
		figter2Btn = fighterElements[1].childNodes[2].childNodes[6];

		// додаємо id і class до кнопок удару і апендимо інформацію про бійця
		figter1Btn.setAttribute('id', 'btnFigter1');
		figter2Btn.setAttribute('id', 'btnFigter2');
		figter1Btn.setAttribute('class', 'punch');
		figter2Btn.setAttribute('class', 'punch');
		divFigter1Info.setAttribute('class', 'div-fighter1-info');
		divFigter2Info.setAttribute('class', 'div-fighter2-info');
		this.overlay.append(this.element);
		
		this.overlay.addEventListener('click', () => {});

		figter1BtnId = document.getElementById('btnFigter1');
		figter2BtnId = document.getElementById('btnFigter2');

		// вішаємо на кнопки обработчики

		figter1BtnId.addEventListener('click', e => {
			const fighter2Health = fighterElements[1].childNodes[2].childNodes[1];
			figter1BtnId.disabled = true;
			figter2BtnId.disabled = false;
			this.fight(...this.state, fighter2Health);
			e.stopPropagation();
		});
		figter2BtnId.addEventListener('click', e => {
			const arrCopy = [];
			const fighter1Health = fighterElements[0].childNodes[2].childNodes[1];
			let arrReverse;
			figter2BtnId.disabled = true;
			figter1BtnId.disabled = false;
			for (let i = 0; i < this.state.length; i++) {
				arrCopy.push(this.state[i]);
			}
			arrReverse = arrCopy.reverse();
			this.fight(...arrReverse, fighter1Health);
			e.stopPropagation();
		});
	}
	// створюємо функцію fight яка генерує удар за допомогою кліку по кнопці
	fight(fighter1, fighter2, health) {
		const hitPowerFighter1 = fighter1.getHitPower();
		const blockPowerFighter2 = fighter2.getBlockPower();
		let result;
		if (hitPowerFighter1 <= blockPowerFighter2) {
			result = 0;
		} else {
			result = hitPowerFighter1 - blockPowerFighter2;
		}
		console.log(result);

		fighter2.health -= result;
		if (fighter2.health == 0 || fighter2.health < 0) {
			const divWinner = document.createElement('div');
			const winner = document.createElement('span');
			const submitelement = document.createElement('button');
			let btnIdEnd;

			health.innerHTML = 0;
			winner.setAttribute('class', 'winner');
			winner.innerHTML = `${fighter1.name} Wins`;
			divWinner.appendChild(winner);

			submitelement.setAttribute('type', 'button');
			submitelement.setAttribute('name', 'chooseFighter');
			submitelement.setAttribute('id', 'end');
			submitelement.innerText = 'Game Over';
			divWinner.appendChild(submitelement);

			popup.open(divWinner.innerHTML);
			btnIdEnd = document.getElementById('end');
			// eslint-disable-next-line no-unused-vars
			btnIdEnd.addEventListener('click', e => {
				window.location.reload();
			});
		}
		console.log(fighter2.health);
		health.innerHTML = fighter2.health;
	}
}

export default Arena;
