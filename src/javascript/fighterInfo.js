// створюємо клас Info для відображення характеристик файтера на арені
class Info {
	constructor(fighterInfo) {
		this.fighterInfo = fighterInfo;
		this.createFighterInfo = this.createFighterInfo.bind(this);
	}

	createFighterInfo(fighterInfo) {
		const divFighterInfo = document.createElement('div');
		//   divFighterInfo.setAttribute("class", "div-fighter-info");

		// Create  spans for  Health
		const health = document.createElement('span');
		health.setAttribute('class', 'info');
		health.innerHTML = 'Health: ';
		divFighterInfo.appendChild(health);

		const healthValue = document.createElement('span');
		healthValue.setAttribute('class', 'value');
		healthValue.innerHTML = fighterInfo.health;
		divFighterInfo.appendChild(healthValue);

		// Create  spans for  Attack
		const attack = document.createElement('span');
		attack.setAttribute('class', 'info');
		attack.innerHTML = 'Attack: ';
		divFighterInfo.appendChild(attack);

		const attackValue = document.createElement('span');
		attackValue.setAttribute('class', 'valueAttack');
		attackValue.innerHTML = fighterInfo.attack;
		divFighterInfo.appendChild(attackValue);

		// Create spans for  Defense
		const defense = document.createElement('span');
		defense.setAttribute('class', 'info');
		defense.innerHTML = 'Defense: ';
		divFighterInfo.appendChild(defense);

		const defenseValue = document.createElement('span');
		defenseValue.setAttribute('class', 'value');
		defenseValue.innerHTML = fighterInfo.defense;
		divFighterInfo.appendChild(defenseValue);

		// Create punch btn
		const submitelement = document.createElement('button');
		submitelement.setAttribute('type', 'button');
		submitelement.setAttribute('name', 'chooseFighter');
		submitelement.innerText = 'Punch';
		divFighterInfo.appendChild(submitelement);

		return divFighterInfo;
	}
}

export default Info;
