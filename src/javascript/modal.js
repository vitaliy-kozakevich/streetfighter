class Popup {
	constructor(options) {
		this.modal = document.querySelector(options.modal);
		this.overlay = document.querySelector(options.overlay);

		this.open = this.open.bind(this);
		this.close = this.close.bind(this);
		this.overlay.addEventListener('click', this.close);
		this.createForm = this.createForm.bind(this);
	}

	open(content) {
		this.modal.innerHTML = content;
		this.modal.classList.add('open');
		this.overlay.classList.add('open');
	}

	close() {
		this.modal.classList.remove('open');
		this.overlay.classList.remove('open');
	}
	createForm(fighterInfo) {
		const divForm = document.createElement('div');
		const form = document.createElement('form');
		divForm.setAttribute('class', 'for-fighter-popup');
		form.setAttribute('class', 'fighter-popup');
		divForm.appendChild(form);

		// Create  Field for Name
		const fighterName = document.createElement('h2');
		fighterName.innerHTML = fighterInfo.name;
		form.appendChild(fighterName);

		const line = document.createElement('hr');
		form.appendChild(line);

		const linebreak = document.createElement('br');
		form.appendChild(linebreak);

		// Create  row for new Health
		const health = document.createElement('span');
		health.setAttribute('class', 'info');
		health.innerHTML = 'Health: ';
		form.appendChild(health);

		const healthValue = document.createElement('span');
		healthValue.setAttribute('class', 'value');
		healthValue.innerHTML = fighterInfo.health;
		form.appendChild(healthValue);

		const healthLabel = document.createElement('label');
		healthLabel.setAttribute('class', 'new-value');
		healthLabel.innerHTML = 'New Health: ';
		form.appendChild(healthLabel);

		const inputHealthElement = document.createElement('input');
		inputHealthElement.setAttribute('type', 'number');
		inputHealthElement.setAttribute('id', 'health');
		inputHealthElement.setAttribute('name', 'health');
		form.appendChild(inputHealthElement);
		form.appendChild(linebreak);

		// Create  row for new Attack
		const attack = document.createElement('span');
		attack.setAttribute('class', 'info');
		attack.innerHTML = 'Attack: ';
		form.appendChild(attack);

		const attackValue = document.createElement('span');
		attackValue.setAttribute('class', 'valueAttack');
		attackValue.innerHTML = fighterInfo.attack;
		form.appendChild(attackValue);

		const attackLabel = document.createElement('label');
		attackLabel.setAttribute('class', 'new-value');
		attackLabel.innerHTML = 'New Attack: ';
		form.appendChild(attackLabel);

		const inputAttackElement = document.createElement('input');
		inputAttackElement.setAttribute('type', 'number');
		inputAttackElement.setAttribute('id', 'attack');
		inputAttackElement.setAttribute('name', 'attack');
		form.appendChild(inputAttackElement);
		form.appendChild(linebreak);

		// Create  row for new Defense
		const defense = document.createElement('span');
		defense.setAttribute('class', 'info');
		defense.innerHTML = 'Defense: ';
		form.appendChild(defense);

		const defenseValue = document.createElement('span');
		defenseValue.setAttribute('class', 'value');
		defenseValue.innerHTML = fighterInfo.defense;
		form.appendChild(defenseValue);

		const defenseLabel = document.createElement('label');
		defenseLabel.setAttribute('class', 'new-value');
		defenseLabel.innerHTML = 'New Defense: ';
		form.appendChild(defenseLabel);

		const inputDefenseElement = document.createElement('input');
		inputDefenseElement.setAttribute('type', 'number');
		inputDefenseElement.setAttribute('id', 'defense');
		inputDefenseElement.setAttribute('name', 'defense');
		form.appendChild(inputDefenseElement);
		form.appendChild(linebreak);

		// Append checkbox
		const checkboxLabel = document.createElement('label');
		checkboxLabel.setAttribute('class', 'checkbox-text');
		checkboxLabel.innerHTML = 'Update Fighter ';
		form.appendChild(checkboxLabel);

		const inputCheckbox = document.createElement('input');
		inputCheckbox.setAttribute('type', 'checkbox');
		inputCheckbox.setAttribute('class', 'checkbox-icon');
		inputCheckbox.setAttribute('id', 'checkbox');
		form.appendChild(inputCheckbox);

		// Append Submit Button
		const submitelement = document.createElement('button');
		submitelement.setAttribute('type', 'button');
		submitelement.setAttribute('name', 'chooseFighter');
		submitelement.setAttribute('id', 'btnChoose');
		submitelement.innerText = 'Choose Fighter';
		form.appendChild(submitelement);

		return divForm;
	}
}

export const popup = new Popup({
	modal: '.modal',
	overlay: '.overlay',
});
