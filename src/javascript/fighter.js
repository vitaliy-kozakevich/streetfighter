class Fighter {
	constructor(name, health, attack, defense, source) {
		this.name = name;
		this.health = health;
		this.attack = attack;
		this.defense = defense;
		this.source = source;

		this.getHitPower = this.getHitPower.bind(this);
		this.getBlockPower = this.getBlockPower.bind(this);
	}

	getHitPower(min = 1, max = 2) {
		const criticalHitChance = Math.floor(min + Math.random() * (max + 1 - min));
		const powerHit = this.attack * criticalHitChance;
		return powerHit;
	}
	getBlockPower(min = 1, max = 2) {
		const dodgeChance = Math.floor(min + Math.random() * (max + 1 - min));
		const powerBlock = this.defense * dodgeChance;
		return powerBlock;
	}
}

export default Fighter;
