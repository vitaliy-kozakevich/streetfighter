import View from './view';
import Fighter from './fighter';
import Arena from './arena';
import FighterView from './fighterView';
import { fighterService } from './services/fightersService';
import { popup } from './modal';

class FightersView extends View {
	constructor(fighters) {
		super();
		this.state = [];

		// створюємо кнопку старту і вішаємо на неї колбек з відображенням арени
		this.startGameBtn = document.getElementById('startBtn');
		this.callArena = new Arena(
			{
				overlay: '.overlay',
			},
			this.state
		);

		this.startGameBtn.addEventListener('click', () => {
			this.callArena.open(this.state);
		});
		this.handleClick = this.handleFighterClick.bind(this);
		this.createFighters(fighters);
	}

  fightersDetailsMap = new Map();

  createFighters(fighters) {
  	const fighterElements = fighters.map(fighter => {
  		const fighterView = new FighterView(fighter, this.handleClick);
  		return fighterView.element;
  	});

  	this.element = this.createElement({
  		tagName: 'div',
  		className: 'fighters',
  	});
  	this.element.append(...fighterElements);
  }

  async handleFighterClick(event, fighter) {
  	this.fightersDetailsMap.set(fighter._id, fighter);
  	const idFighter = fighter._id;
  	const info = await fighterService.getFighterDetails(idFighter);
  	let button = null;
  	let checkbox = null;

  	// allow to edit health and power in this modal
  	// show modal with fighter info
  	const form = popup.createForm(info);
  	popup.open(form.innerHTML);
  	button = document.getElementById('btnChoose');
  	checkbox = document.getElementById('checkbox');

  	// створюємо обработчик по кліку на кнопку і пушимо інстанси класу Fighter в масив
  	button.addEventListener('click', () => {
  		const newHealthValue = document.getElementById('health');
  		const newAttackValue = document.getElementById('attack');
  		const newDefensealue = document.getElementById('defense');
  		let fighterInfo = null;
  		fighterInfo = checkbox.checked
  			? new Fighter(
  				info.name,
  				newHealthValue.value,
  				newAttackValue.value,
  				newDefensealue.value,
  				info.source
  			)
  			: new Fighter(
  				info.name,
  				info.health,
  				info.attack,
  				info.defense,
  				info.source
  			);
  		if (this.state.length < 1) {
  			this.state.push(fighterInfo);
  		} else if (this.state.length === 1) {
  			this.state.push(fighterInfo);
  			this.startGameBtn.disabled = false;
  		} else {
  			alert('You cannot add third fighter\nPlease start the game');
  		}
  		popup.close();
  	});
  }
}

export default FightersView;
