/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/dist/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./index.js":
/*!******************!*\
  !*** ./index.js ***!
  \******************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _src_javascript_app__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./src/javascript/app */ "./src/javascript/app.js");
/* harmony import */ var _src_styles_styles_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./src/styles/styles.css */ "./src/styles/styles.css");
/* harmony import */ var _src_styles_styles_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_src_styles_styles_css__WEBPACK_IMPORTED_MODULE_1__);


new _src_javascript_app__WEBPACK_IMPORTED_MODULE_0__["default"]();

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/styles/styles.css":
/*!*********************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/styles/styles.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js")(false);
// Module
exports.push([module.i, "html,\r\nbody, h1, h2 {\r\n    height: 100%;\r\n    width: 100%;\r\n    margin: 0;\r\n    padding: 0;\r\n}\r\n/* Кнопка Start Game  */\r\n#startBtn {\r\n    width: 20%;\r\n    margin: 0 auto;\r\n    margin-top: 30px;\r\n    height: 40px;\r\n    border-radius: 5px;\r\n    font-size: 18px;\r\n    font-weight: 700;\r\n    background-color: #2BC1F2;\r\n    cursor: pointer;\r\n    font-family: 'Ubuntu', sans-serif;\r\n    display: block;\r\n}\r\n#root {\r\n    display: flex;\r\n    flex-direction: column;\r\n    align-items: center;\r\n    justify-content: center;\r\n    height: 90%;\r\n    width: 100%;\r\n    z-index: 1000;\r\n}\r\n\r\n.modal {\r\n    position: fixed;\r\n    width: 25vw;\r\n    height: 35vh;\r\n    margin: 0;\r\n    background: #fff;\r\n    display: none;\r\n    box-sizing: border-box;\r\n    border-radius: 10px;\r\n    padding: 15px;\r\n    z-index: 1001;\r\n}\r\n.overlay{\r\n    position: fixed;\r\n    width: 100%;\r\n    height: 100%;\r\n    color: white;\r\n    top: 0;\r\n    left: 0;\r\n    background: rgba(0, 0, 0, 0.9);\r\n    display: none;\r\n    z-index: 1000;\r\n    cursor: pointer;\r\n}\r\n\r\n/* Стилі для модального вікна  */\r\n.for-fighter-popup{\r\n    position: relative;\r\n    text-align: center;\r\n    border: 1px solid #ccc;\r\n    width: 300px;\r\n    box-shadow: 0 0 15px;\r\n    border-radius: 6px;\r\n    display: none;\r\n    box-sizing: border-box;\r\n}\r\n.fighter-popup h2{\r\n    text-align: center;\r\n}\r\n.info{\r\n    float: left;\r\n    margin: 5px 5px 0 0;\r\n    font-size: 18px;\r\n    display: inline-block;\r\n}\r\n.value{\r\n    float: left;\r\n    margin: 5px 5px 0 0;\r\n    font-size: 18px;\r\n    display: inline-block;\r\n}\r\n.valueAttack{\r\n    float: left;\r\n    margin: 5px 15px 0 0;\r\n    font-size: 18px;\r\n    display: inline-block;\r\n}\r\n.new-value{\r\n    margin: 5px 5px 0 50px;\r\n    font-size: 18px;\r\n    display: inline-block;\r\n}\r\ninput[type=\"number\"]{\r\n    width: 20%;\r\n    padding-left: 2px;\r\n    float: right;\r\n    border-radius: 5px;\r\n    margin: 5px 0 0 0;\r\n    display: inline-block;\r\n    border: 2px solid #2BC1F2;\r\n}\r\n.checkbox-text{\r\n    margin: 40px 0 0 50px;\r\n    font-size: 18px;\r\n    display: inline-block;\r\n}\r\n.checkbox-icon{\r\n    margin-top: 45px;\r\n    float: right;\r\n}\r\n    \r\nbutton[name=\"chooseFighter\"]{\r\n    float: left;\r\n    width: 40%;\r\n    padding: 5x; \r\n    margin-top: 30px;\r\n    color: #fff;\r\n    background-color: #2BC1F2;\r\n    border: none;\r\n    height: 40px;\r\n    border-radius: 5px;\r\n    font-size: 16px;\r\n    font-weight: 700;\r\n    cursor: pointer;\r\n    font-family: 'Ubuntu', sans-serif\r\n}\r\n\r\n.fighters {\r\n    display: flex;\r\n    justify-content: space-between;\r\n    align-items: center;\r\n    flex: 1;\r\n    flex-wrap: wrap;\r\n    padding: 15px 15px;\r\n    /* position: relative; */\r\n}\r\n\r\n\r\n.fighter {\r\n    display: flex;\r\n    flex-direction: column;\r\n    padding: 20px;\r\n}\r\n/* розвертаэмо файтера 2 на 180 гр  */\r\n.fighter-rotate {\r\n    transform: rotateY(180deg);\r\n}\r\n\r\n.fighter:hover {\r\n    box-shadow: 0 0 50px 10px rgba(0,0,0,0.06);\r\n    cursor: pointer;\r\n}\r\n/* розвертаэмо ім\"я та інфо на 180 гр  */\r\n.name {\r\n    align-self: center;\r\n    font-size: 21px;\r\n    margin-top: 20px;\r\n}\r\n.back-rotate {\r\n    transform: rotateY(180deg);\r\n}\r\n\r\n.div-fighter1-info {\r\n    padding-left: 80px;\r\n}\r\n/* розвертаэмо ім\"я та інфо на 180 гр  */\r\n.div-fighter2-info {\r\n    transform: rotateY(180deg);\r\n    padding-left: 80px;\r\n}\r\n\r\n.punch {\r\n    margin-left: 70px;\r\n}\r\n.fighter-image {\r\n    height: 260px;\r\n}\r\n/* Модалка для закічення гри */\r\n.winner {\r\n    display: block;\r\n    font-size: 20px;\r\n    margin-top: 20px;\r\n    text-align: center;\r\n    font-family: 'Ubuntu', sans-serif\r\n}\r\n#end {\r\n    display: block;\r\n    margin-top: 40px;\r\n    margin-left: 90px;\r\n}\r\n\r\n#loading-overlay {\r\n    position: absolute;\r\n    top: 0;\r\n    left: 0;\r\n    width: 100%;\r\n    height: 100%;\r\n    display: flex;\r\n    align-items: center;\r\n    justify-content: center;\r\n    font-size: 18px;\r\n    background: rgba(255, 255, 255, 0.7);\r\n    visibility: hidden;\r\n}\r\n.open {\r\n    display: block; \r\n}\r\n\r\n", ""]);



/***/ }),

/***/ "./node_modules/css-loader/dist/runtime/api.js":
/*!*****************************************************!*\
  !*** ./node_modules/css-loader/dist/runtime/api.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function (useSourceMap) {
  var list = []; // return the list of modules as css string

  list.toString = function toString() {
    return this.map(function (item) {
      var content = cssWithMappingToString(item, useSourceMap);

      if (item[2]) {
        return '@media ' + item[2] + '{' + content + '}';
      } else {
        return content;
      }
    }).join('');
  }; // import a list of modules into the list


  list.i = function (modules, mediaQuery) {
    if (typeof modules === 'string') {
      modules = [[null, modules, '']];
    }

    var alreadyImportedModules = {};

    for (var i = 0; i < this.length; i++) {
      var id = this[i][0];

      if (id != null) {
        alreadyImportedModules[id] = true;
      }
    }

    for (i = 0; i < modules.length; i++) {
      var item = modules[i]; // skip already imported module
      // this implementation is not 100% perfect for weird media query combinations
      // when a module is imported multiple times with different media queries.
      // I hope this will never occur (Hey this way we have smaller bundles)

      if (item[0] == null || !alreadyImportedModules[item[0]]) {
        if (mediaQuery && !item[2]) {
          item[2] = mediaQuery;
        } else if (mediaQuery) {
          item[2] = '(' + item[2] + ') and (' + mediaQuery + ')';
        }

        list.push(item);
      }
    }
  };

  return list;
};

function cssWithMappingToString(item, useSourceMap) {
  var content = item[1] || '';
  var cssMapping = item[3];

  if (!cssMapping) {
    return content;
  }

  if (useSourceMap && typeof btoa === 'function') {
    var sourceMapping = toComment(cssMapping);
    var sourceURLs = cssMapping.sources.map(function (source) {
      return '/*# sourceURL=' + cssMapping.sourceRoot + source + ' */';
    });
    return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
  }

  return [content].join('\n');
} // Adapted from convert-source-map (MIT)


function toComment(sourceMap) {
  // eslint-disable-next-line no-undef
  var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
  var data = 'sourceMappingURL=data:application/json;charset=utf-8;base64,' + base64;
  return '/*# ' + data + ' */';
}

/***/ }),

/***/ "./node_modules/style-loader/lib/addStyles.js":
/*!****************************************************!*\
  !*** ./node_modules/style-loader/lib/addStyles.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/

var stylesInDom = {};

var	memoize = function (fn) {
	var memo;

	return function () {
		if (typeof memo === "undefined") memo = fn.apply(this, arguments);
		return memo;
	};
};

var isOldIE = memoize(function () {
	// Test for IE <= 9 as proposed by Browserhacks
	// @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805
	// Tests for existence of standard globals is to allow style-loader
	// to operate correctly into non-standard environments
	// @see https://github.com/webpack-contrib/style-loader/issues/177
	return window && document && document.all && !window.atob;
});

var getTarget = function (target, parent) {
  if (parent){
    return parent.querySelector(target);
  }
  return document.querySelector(target);
};

var getElement = (function (fn) {
	var memo = {};

	return function(target, parent) {
                // If passing function in options, then use it for resolve "head" element.
                // Useful for Shadow Root style i.e
                // {
                //   insertInto: function () { return document.querySelector("#foo").shadowRoot }
                // }
                if (typeof target === 'function') {
                        return target();
                }
                if (typeof memo[target] === "undefined") {
			var styleTarget = getTarget.call(this, target, parent);
			// Special case to return head of iframe instead of iframe itself
			if (window.HTMLIFrameElement && styleTarget instanceof window.HTMLIFrameElement) {
				try {
					// This will throw an exception if access to iframe is blocked
					// due to cross-origin restrictions
					styleTarget = styleTarget.contentDocument.head;
				} catch(e) {
					styleTarget = null;
				}
			}
			memo[target] = styleTarget;
		}
		return memo[target]
	};
})();

var singleton = null;
var	singletonCounter = 0;
var	stylesInsertedAtTop = [];

var	fixUrls = __webpack_require__(/*! ./urls */ "./node_modules/style-loader/lib/urls.js");

module.exports = function(list, options) {
	if (typeof DEBUG !== "undefined" && DEBUG) {
		if (typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
	}

	options = options || {};

	options.attrs = typeof options.attrs === "object" ? options.attrs : {};

	// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
	// tags it will allow on a page
	if (!options.singleton && typeof options.singleton !== "boolean") options.singleton = isOldIE();

	// By default, add <style> tags to the <head> element
        if (!options.insertInto) options.insertInto = "head";

	// By default, add <style> tags to the bottom of the target
	if (!options.insertAt) options.insertAt = "bottom";

	var styles = listToStyles(list, options);

	addStylesToDom(styles, options);

	return function update (newList) {
		var mayRemove = [];

		for (var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];

			domStyle.refs--;
			mayRemove.push(domStyle);
		}

		if(newList) {
			var newStyles = listToStyles(newList, options);
			addStylesToDom(newStyles, options);
		}

		for (var i = 0; i < mayRemove.length; i++) {
			var domStyle = mayRemove[i];

			if(domStyle.refs === 0) {
				for (var j = 0; j < domStyle.parts.length; j++) domStyle.parts[j]();

				delete stylesInDom[domStyle.id];
			}
		}
	};
};

function addStylesToDom (styles, options) {
	for (var i = 0; i < styles.length; i++) {
		var item = styles[i];
		var domStyle = stylesInDom[item.id];

		if(domStyle) {
			domStyle.refs++;

			for(var j = 0; j < domStyle.parts.length; j++) {
				domStyle.parts[j](item.parts[j]);
			}

			for(; j < item.parts.length; j++) {
				domStyle.parts.push(addStyle(item.parts[j], options));
			}
		} else {
			var parts = [];

			for(var j = 0; j < item.parts.length; j++) {
				parts.push(addStyle(item.parts[j], options));
			}

			stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
		}
	}
}

function listToStyles (list, options) {
	var styles = [];
	var newStyles = {};

	for (var i = 0; i < list.length; i++) {
		var item = list[i];
		var id = options.base ? item[0] + options.base : item[0];
		var css = item[1];
		var media = item[2];
		var sourceMap = item[3];
		var part = {css: css, media: media, sourceMap: sourceMap};

		if(!newStyles[id]) styles.push(newStyles[id] = {id: id, parts: [part]});
		else newStyles[id].parts.push(part);
	}

	return styles;
}

function insertStyleElement (options, style) {
	var target = getElement(options.insertInto)

	if (!target) {
		throw new Error("Couldn't find a style target. This probably means that the value for the 'insertInto' parameter is invalid.");
	}

	var lastStyleElementInsertedAtTop = stylesInsertedAtTop[stylesInsertedAtTop.length - 1];

	if (options.insertAt === "top") {
		if (!lastStyleElementInsertedAtTop) {
			target.insertBefore(style, target.firstChild);
		} else if (lastStyleElementInsertedAtTop.nextSibling) {
			target.insertBefore(style, lastStyleElementInsertedAtTop.nextSibling);
		} else {
			target.appendChild(style);
		}
		stylesInsertedAtTop.push(style);
	} else if (options.insertAt === "bottom") {
		target.appendChild(style);
	} else if (typeof options.insertAt === "object" && options.insertAt.before) {
		var nextSibling = getElement(options.insertAt.before, target);
		target.insertBefore(style, nextSibling);
	} else {
		throw new Error("[Style Loader]\n\n Invalid value for parameter 'insertAt' ('options.insertAt') found.\n Must be 'top', 'bottom', or Object.\n (https://github.com/webpack-contrib/style-loader#insertat)\n");
	}
}

function removeStyleElement (style) {
	if (style.parentNode === null) return false;
	style.parentNode.removeChild(style);

	var idx = stylesInsertedAtTop.indexOf(style);
	if(idx >= 0) {
		stylesInsertedAtTop.splice(idx, 1);
	}
}

function createStyleElement (options) {
	var style = document.createElement("style");

	if(options.attrs.type === undefined) {
		options.attrs.type = "text/css";
	}

	if(options.attrs.nonce === undefined) {
		var nonce = getNonce();
		if (nonce) {
			options.attrs.nonce = nonce;
		}
	}

	addAttrs(style, options.attrs);
	insertStyleElement(options, style);

	return style;
}

function createLinkElement (options) {
	var link = document.createElement("link");

	if(options.attrs.type === undefined) {
		options.attrs.type = "text/css";
	}
	options.attrs.rel = "stylesheet";

	addAttrs(link, options.attrs);
	insertStyleElement(options, link);

	return link;
}

function addAttrs (el, attrs) {
	Object.keys(attrs).forEach(function (key) {
		el.setAttribute(key, attrs[key]);
	});
}

function getNonce() {
	if (false) {}

	return __webpack_require__.nc;
}

function addStyle (obj, options) {
	var style, update, remove, result;

	// If a transform function was defined, run it on the css
	if (options.transform && obj.css) {
	    result = typeof options.transform === 'function'
		 ? options.transform(obj.css) 
		 : options.transform.default(obj.css);

	    if (result) {
	    	// If transform returns a value, use that instead of the original css.
	    	// This allows running runtime transformations on the css.
	    	obj.css = result;
	    } else {
	    	// If the transform function returns a falsy value, don't add this css.
	    	// This allows conditional loading of css
	    	return function() {
	    		// noop
	    	};
	    }
	}

	if (options.singleton) {
		var styleIndex = singletonCounter++;

		style = singleton || (singleton = createStyleElement(options));

		update = applyToSingletonTag.bind(null, style, styleIndex, false);
		remove = applyToSingletonTag.bind(null, style, styleIndex, true);

	} else if (
		obj.sourceMap &&
		typeof URL === "function" &&
		typeof URL.createObjectURL === "function" &&
		typeof URL.revokeObjectURL === "function" &&
		typeof Blob === "function" &&
		typeof btoa === "function"
	) {
		style = createLinkElement(options);
		update = updateLink.bind(null, style, options);
		remove = function () {
			removeStyleElement(style);

			if(style.href) URL.revokeObjectURL(style.href);
		};
	} else {
		style = createStyleElement(options);
		update = applyToTag.bind(null, style);
		remove = function () {
			removeStyleElement(style);
		};
	}

	update(obj);

	return function updateStyle (newObj) {
		if (newObj) {
			if (
				newObj.css === obj.css &&
				newObj.media === obj.media &&
				newObj.sourceMap === obj.sourceMap
			) {
				return;
			}

			update(obj = newObj);
		} else {
			remove();
		}
	};
}

var replaceText = (function () {
	var textStore = [];

	return function (index, replacement) {
		textStore[index] = replacement;

		return textStore.filter(Boolean).join('\n');
	};
})();

function applyToSingletonTag (style, index, remove, obj) {
	var css = remove ? "" : obj.css;

	if (style.styleSheet) {
		style.styleSheet.cssText = replaceText(index, css);
	} else {
		var cssNode = document.createTextNode(css);
		var childNodes = style.childNodes;

		if (childNodes[index]) style.removeChild(childNodes[index]);

		if (childNodes.length) {
			style.insertBefore(cssNode, childNodes[index]);
		} else {
			style.appendChild(cssNode);
		}
	}
}

function applyToTag (style, obj) {
	var css = obj.css;
	var media = obj.media;

	if(media) {
		style.setAttribute("media", media)
	}

	if(style.styleSheet) {
		style.styleSheet.cssText = css;
	} else {
		while(style.firstChild) {
			style.removeChild(style.firstChild);
		}

		style.appendChild(document.createTextNode(css));
	}
}

function updateLink (link, options, obj) {
	var css = obj.css;
	var sourceMap = obj.sourceMap;

	/*
		If convertToAbsoluteUrls isn't defined, but sourcemaps are enabled
		and there is no publicPath defined then lets turn convertToAbsoluteUrls
		on by default.  Otherwise default to the convertToAbsoluteUrls option
		directly
	*/
	var autoFixUrls = options.convertToAbsoluteUrls === undefined && sourceMap;

	if (options.convertToAbsoluteUrls || autoFixUrls) {
		css = fixUrls(css);
	}

	if (sourceMap) {
		// http://stackoverflow.com/a/26603875
		css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
	}

	var blob = new Blob([css], { type: "text/css" });

	var oldSrc = link.href;

	link.href = URL.createObjectURL(blob);

	if(oldSrc) URL.revokeObjectURL(oldSrc);
}


/***/ }),

/***/ "./node_modules/style-loader/lib/urls.js":
/*!***********************************************!*\
  !*** ./node_modules/style-loader/lib/urls.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {


/**
 * When source maps are enabled, `style-loader` uses a link element with a data-uri to
 * embed the css on the page. This breaks all relative urls because now they are relative to a
 * bundle instead of the current page.
 *
 * One solution is to only use full urls, but that may be impossible.
 *
 * Instead, this function "fixes" the relative urls to be absolute according to the current page location.
 *
 * A rudimentary test suite is located at `test/fixUrls.js` and can be run via the `npm test` command.
 *
 */

module.exports = function (css) {
  // get current location
  var location = typeof window !== "undefined" && window.location;

  if (!location) {
    throw new Error("fixUrls requires window.location");
  }

	// blank or null?
	if (!css || typeof css !== "string") {
	  return css;
  }

  var baseUrl = location.protocol + "//" + location.host;
  var currentDir = baseUrl + location.pathname.replace(/\/[^\/]*$/, "/");

	// convert each url(...)
	/*
	This regular expression is just a way to recursively match brackets within
	a string.

	 /url\s*\(  = Match on the word "url" with any whitespace after it and then a parens
	   (  = Start a capturing group
	     (?:  = Start a non-capturing group
	         [^)(]  = Match anything that isn't a parentheses
	         |  = OR
	         \(  = Match a start parentheses
	             (?:  = Start another non-capturing groups
	                 [^)(]+  = Match anything that isn't a parentheses
	                 |  = OR
	                 \(  = Match a start parentheses
	                     [^)(]*  = Match anything that isn't a parentheses
	                 \)  = Match a end parentheses
	             )  = End Group
              *\) = Match anything and then a close parens
          )  = Close non-capturing group
          *  = Match anything
       )  = Close capturing group
	 \)  = Match a close parens

	 /gi  = Get all matches, not the first.  Be case insensitive.
	 */
	var fixedCss = css.replace(/url\s*\(((?:[^)(]|\((?:[^)(]+|\([^)(]*\))*\))*)\)/gi, function(fullMatch, origUrl) {
		// strip quotes (if they exist)
		var unquotedOrigUrl = origUrl
			.trim()
			.replace(/^"(.*)"$/, function(o, $1){ return $1; })
			.replace(/^'(.*)'$/, function(o, $1){ return $1; });

		// already a full url? no change
		if (/^(#|data:|http:\/\/|https:\/\/|file:\/\/\/|\s*$)/i.test(unquotedOrigUrl)) {
		  return fullMatch;
		}

		// convert the url to a full url
		var newUrl;

		if (unquotedOrigUrl.indexOf("//") === 0) {
		  	//TODO: should we add protocol?
			newUrl = unquotedOrigUrl;
		} else if (unquotedOrigUrl.indexOf("/") === 0) {
			// path should be relative to the base url
			newUrl = baseUrl + unquotedOrigUrl; // already starts with '/'
		} else {
			// path should be relative to current directory
			newUrl = currentDir + unquotedOrigUrl.replace(/^\.\//, ""); // Strip leading './'
		}

		// send back the fixed url(...)
		return "url(" + JSON.stringify(newUrl) + ")";
	});

	// send back the fixed css
	return fixedCss;
};


/***/ }),

/***/ "./src/javascript/app.js":
/*!*******************************!*\
  !*** ./src/javascript/app.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _fightersView__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./fightersView */ "./src/javascript/fightersView.js");
/* harmony import */ var _services_fightersService__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./services/fightersService */ "./src/javascript/services/fightersService.js");
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }




class App {
  constructor() {
    this.startApp();
  }

  async startApp() {
    try {
      App.loadingElement.style.visibility = 'visible';
      const fighters = await _services_fightersService__WEBPACK_IMPORTED_MODULE_1__["fighterService"].getFighters();
      const fightersView = new _fightersView__WEBPACK_IMPORTED_MODULE_0__["default"](fighters);
      const fightersElement = fightersView.element;
      App.rootElement.appendChild(fightersElement);
    } catch (error) {
      console.warn(error);
      App.rootElement.innerText = 'Failed to load data';
    } finally {
      App.loadingElement.style.visibility = 'hidden';
    }
  }

}

_defineProperty(App, "rootElement", document.getElementById('root'));

_defineProperty(App, "loadingElement", document.getElementById('loading-overlay'));

/* harmony default export */ __webpack_exports__["default"] = (App);

/***/ }),

/***/ "./src/javascript/arena.js":
/*!*********************************!*\
  !*** ./src/javascript/arena.js ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _fighterInfo__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./fighterInfo */ "./src/javascript/fighterInfo.js");
/* harmony import */ var _view__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./view */ "./src/javascript/view.js");
/* harmony import */ var _fighterView__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./fighterView */ "./src/javascript/fighterView.js");
/* harmony import */ var _modal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./modal */ "./src/javascript/modal.js");



 // створюємо клас Арена для відображення бійки

class Arena extends _view__WEBPACK_IMPORTED_MODULE_1__["default"] {
  constructor(options, state) {
    super();
    this.state = state;
    this.overlay = document.querySelector(options.overlay);
    this.fighterInfoClass = new _fighterInfo__WEBPACK_IMPORTED_MODULE_0__["default"]();
    this.open = this.open.bind(this);
    this.fightArena = this.fightArena.bind(this);
    this.fight = this.fight.bind(this);
  } // відкриття Арени


  open(state) {
    this.overlay.classList.add('open');
    this.fightArena(state);
  }

  fightArena(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new _fighterView__WEBPACK_IMPORTED_MODULE_2__["default"](fighter, e => {});
      return fighterView.element;
    });
    let figter1Btn;
    let figter2Btn;
    let figter1BtnId;
    let figter2BtnId;
    let divFigter1Info;
    let divFigter2Info;
    let divFighterInfo; // створюємо клас 'fighters' і поміщаємо туди бійців, додаємо класи для другого бійця для розвороту на 180 градусів

    this.element = this.createElement({
      tagName: 'div',
      className: 'fighters'
    });
    this.element.append(...fighterElements);
    fighterElements[1].setAttribute('class', 'fighter fighter-rotate');
    fighterElements[1].childNodes[1].setAttribute('class', 'name back-rotate');

    for (let i = 0; i < this.state.length; i++) {
      divFighterInfo = this.fighterInfoClass.createFighterInfo(this.state[i]);
      fighterElements[i].append(divFighterInfo);
    }

    divFigter1Info = fighterElements[0].childNodes[2];
    divFigter2Info = fighterElements[1].childNodes[2];
    figter1Btn = fighterElements[0].childNodes[2].childNodes[6];
    figter2Btn = fighterElements[1].childNodes[2].childNodes[6]; // додаємо id і class до кнопок удару і апендимо інформацію про бійця

    figter1Btn.setAttribute('id', 'btnFigter1');
    figter2Btn.setAttribute('id', 'btnFigter2');
    figter1Btn.setAttribute('class', 'punch');
    figter2Btn.setAttribute('class', 'punch');
    divFigter1Info.setAttribute('class', 'div-fighter1-info');
    divFigter2Info.setAttribute('class', 'div-fighter2-info');
    this.overlay.append(this.element);
    figter1BtnId = document.getElementById('btnFigter1');
    figter2BtnId = document.getElementById('btnFigter2'); // вішаємо на кнопки обработчики

    figter1BtnId.addEventListener('click', e => {
      const fighter2Health = fighterElements[1].childNodes[2].childNodes[1];
      figter1BtnId.disabled = true;
      figter2BtnId.disabled = false;
      this.fight(...this.state, fighter2Health);
      e.stopPropagation();
    });
    figter2BtnId.addEventListener('click', e => {
      const arrCopy = [];
      const fighter1Health = fighterElements[0].childNodes[2].childNodes[1];
      let arrReverse;
      figter2BtnId.disabled = true;
      figter1BtnId.disabled = false;

      for (let i = 0; i < this.state.length; i++) {
        arrCopy.push(this.state[i]);
      }

      arrReverse = arrCopy.reverse();
      this.fight(...arrReverse, fighter1Health);
      e.stopPropagation();
    });
  } // створюємо функцію fight яка приймає бійців в якості параметрів та генерує удар за допомогою кліку по кнопці


  fight(fighter1, fighter2, health) {
    const hitPowerFighter1 = fighter1.getHitPower();
    const blockPowerFighter2 = fighter2.getBlockPower();
    let result;

    if (hitPowerFighter1 <= blockPowerFighter2) {
      result = 0;
    } else {
      result = hitPowerFighter1 - blockPowerFighter2;
    }

    console.log(result);
    fighter2.health -= result;

    if (fighter2.health == 0 || fighter2.health < 0) {
      const divWinner = document.createElement('div');
      const winner = document.createElement('span');
      const submitelement = document.createElement('button');
      let btnIdEnd;
      health.innerHTML = 0;
      figter1BtnId.disabled = true;
      figter2BtnId.disabled = true;
      winner.setAttribute('class', 'winner');
      winner.innerHTML = `${fighter1.name} Wins`;
      divWinner.appendChild(winner);
      submitelement.setAttribute('type', 'button');
      submitelement.setAttribute('name', 'chooseFighter');
      submitelement.setAttribute('id', 'end');
      submitelement.innerText = 'Game Over';
      divWinner.appendChild(submitelement);
      _modal__WEBPACK_IMPORTED_MODULE_3__["popup"].open(divWinner.innerHTML);
      btnIdEnd = document.getElementById('end');
      btnIdEnd.addEventListener('click', e => {
        window.location.reload();
      });
    }

    console.log(fighter2.health);
    health.innerHTML = fighter2.health;
  }

}

/* harmony default export */ __webpack_exports__["default"] = (Arena);

/***/ }),

/***/ "./src/javascript/fighter.js":
/*!***********************************!*\
  !*** ./src/javascript/fighter.js ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
class Fighter {
  constructor(name, health, attack, defense, source) {
    this.name = name;
    this.health = health;
    this.attack = attack;
    this.defense = defense;
    this.source = source;
    this.getHitPower = this.getHitPower.bind(this);
    this.getBlockPower = this.getBlockPower.bind(this);
  }

  getHitPower(min = 1, max = 2) {
    const criticalHitChance = Math.floor(min + Math.random() * (max + 1 - min));
    const powerHit = this.attack * criticalHitChance;
    return powerHit;
  }

  getBlockPower(min = 1, max = 2) {
    const dodgeChance = Math.floor(min + Math.random() * (max + 1 - min));
    const powerBlock = this.defense * dodgeChance;
    return powerBlock;
  }

}

/* harmony default export */ __webpack_exports__["default"] = (Fighter);

/***/ }),

/***/ "./src/javascript/fighterInfo.js":
/*!***************************************!*\
  !*** ./src/javascript/fighterInfo.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
// створюємо клас Info для відображення характеристик файтера на арені
class Info {
  constructor(fighterInfo) {
    this.fighterInfo = fighterInfo;
    this.createFighterInfo = this.createFighterInfo.bind(this);
  }

  createFighterInfo(fighterInfo) {
    const divFighterInfo = document.createElement('div'); //   divFighterInfo.setAttribute("class", "div-fighter-info");
    // Create  spans for  Health

    const health = document.createElement('span');
    health.setAttribute('class', 'info');
    health.innerHTML = 'Health: ';
    divFighterInfo.appendChild(health);
    const healthValue = document.createElement('span');
    healthValue.setAttribute('class', 'value');
    healthValue.innerHTML = fighterInfo.health;
    divFighterInfo.appendChild(healthValue); // Create  spans for  Attack

    const attack = document.createElement('span');
    attack.setAttribute('class', 'info');
    attack.innerHTML = 'Attack: ';
    divFighterInfo.appendChild(attack);
    const attackValue = document.createElement('span');
    attackValue.setAttribute('class', 'valueAttack');
    attackValue.innerHTML = fighterInfo.attack;
    divFighterInfo.appendChild(attackValue); // Create spans for  Defense

    const defense = document.createElement('span');
    defense.setAttribute('class', 'info');
    defense.innerHTML = 'Defense: ';
    divFighterInfo.appendChild(defense);
    const defenseValue = document.createElement('span');
    defenseValue.setAttribute('class', 'value');
    defenseValue.innerHTML = fighterInfo.defense;
    divFighterInfo.appendChild(defenseValue); // Create punch btn

    const submitelement = document.createElement('button');
    submitelement.setAttribute('type', 'button');
    submitelement.setAttribute('name', 'chooseFighter');
    submitelement.innerText = 'Punch';
    divFighterInfo.appendChild(submitelement);
    return divFighterInfo;
  }

}

/* harmony default export */ __webpack_exports__["default"] = (Info);

/***/ }),

/***/ "./src/javascript/fighterView.js":
/*!***************************************!*\
  !*** ./src/javascript/fighterView.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _view__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./view */ "./src/javascript/view.js");


class FighterView extends _view__WEBPACK_IMPORTED_MODULE_0__["default"] {
  constructor(fighter, handleClick) {
    super();
    this.createFighter(fighter, handleClick);
  }

  createFighter(fighter, handleClick) {
    const {
      name,
      source
    } = fighter;
    const nameElement = this.createName(name);
    const imageElement = this.createImage(source);
    this.element = this.createElement({
      tagName: 'div',
      className: 'fighter'
    });
    this.element.append(imageElement, nameElement);
    this.element.addEventListener('click', event => handleClick(event, fighter), false);
  }

  createName(name) {
    const nameElement = this.createElement({
      tagName: 'span',
      className: 'name'
    });
    nameElement.innerText = name;
    return nameElement;
  }

  createImage(source) {
    const attributes = {
      src: source
    };
    const imgElement = this.createElement({
      tagName: 'img',
      className: 'fighter-image',
      attributes
    });
    return imgElement;
  }

}

/* harmony default export */ __webpack_exports__["default"] = (FighterView);

/***/ }),

/***/ "./src/javascript/fightersView.js":
/*!****************************************!*\
  !*** ./src/javascript/fightersView.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _view__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./view */ "./src/javascript/view.js");
/* harmony import */ var _fighter__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./fighter */ "./src/javascript/fighter.js");
/* harmony import */ var _arena__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./arena */ "./src/javascript/arena.js");
/* harmony import */ var _fighterView__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./fighterView */ "./src/javascript/fighterView.js");
/* harmony import */ var _services_fightersService__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./services/fightersService */ "./src/javascript/services/fightersService.js");
/* harmony import */ var _modal__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./modal */ "./src/javascript/modal.js");
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }








class FightersView extends _view__WEBPACK_IMPORTED_MODULE_0__["default"] {
  constructor(fighters) {
    super();

    _defineProperty(this, "fightersDetailsMap", new Map());

    this.state = []; // створюємо кнопку старту і вішаємо на неї колбек з відображенням арени

    this.startGameBtn = document.getElementById('startBtn');
    this.callArena = new _arena__WEBPACK_IMPORTED_MODULE_2__["default"]({
      overlay: '.overlay'
    }, this.state);
    this.startGameBtn.addEventListener('click', () => {
      this.callArena.open(this.state);
    });
    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);
  }

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new _fighterView__WEBPACK_IMPORTED_MODULE_3__["default"](fighter, this.handleClick);
      return fighterView.element;
    });
    this.element = this.createElement({
      tagName: 'div',
      className: 'fighters'
    });
    this.element.append(...fighterElements);
  }

  async handleFighterClick(event, fighter) {
    this.fightersDetailsMap.set(fighter._id, fighter);
    const idFighter = fighter._id;
    const info = await _services_fightersService__WEBPACK_IMPORTED_MODULE_4__["fighterService"].getFighterDetails(idFighter);
    let button = null;
    let checkbox = null; // allow to edit health and power in this modal
    // show modal with fighter info

    const form = _modal__WEBPACK_IMPORTED_MODULE_5__["popup"].createForm(info);
    _modal__WEBPACK_IMPORTED_MODULE_5__["popup"].open(form.innerHTML);
    button = document.getElementById('btnChoose');
    checkbox = document.getElementById('checkbox'); // створюємо обработчик по кліку на кнопку і пушимо інстанси класу Fighter в масив

    button.addEventListener('click', e => {
      const newHealthValue = document.getElementById('health');
      const newAttackValue = document.getElementById('attack');
      const newDefensealue = document.getElementById('defense');
      let fighter = null;
      fighter = checkbox.checked ? new _fighter__WEBPACK_IMPORTED_MODULE_1__["default"](info.name, newHealthValue.value, newAttackValue.value, newDefensealue.value, info.source) : new _fighter__WEBPACK_IMPORTED_MODULE_1__["default"](info.name, info.health, info.attack, info.defense, info.source);

      if (this.state.length < 1) {
        this.state.push(fighter);
      } else if (this.state.length === 1) {
        this.state.push(fighter);
        startBtn.disabled = false;
      } else {
        alert('You cannot add third fighter\nPlease start the game');
      }

      _modal__WEBPACK_IMPORTED_MODULE_5__["popup"].close();
    });
  }

}

/* harmony default export */ __webpack_exports__["default"] = (FightersView);

/***/ }),

/***/ "./src/javascript/helpers/apiHelper.js":
/*!*********************************************!*\
  !*** ./src/javascript/helpers/apiHelper.js ***!
  \*********************************************/
/*! exports provided: callApi */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "callApi", function() { return callApi; });
const API_URL = 'https://api.github.com/repos/binary-studio-academy/stage-2-es6-for-everyone/contents/resources/api/';

function callApi(endpoind, method) {
  const url = API_URL + endpoind;
  const options = {
    method
  };
  return fetch(url, options).then(response => response.ok ? response.json() : Promise.reject(Error('Failed to load'))).catch(error => {
    throw error;
  });
}



/***/ }),

/***/ "./src/javascript/modal.js":
/*!*********************************!*\
  !*** ./src/javascript/modal.js ***!
  \*********************************/
/*! exports provided: popup */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "popup", function() { return popup; });
class Popup {
  constructor(options) {
    this.modal = document.querySelector(options.modal);
    this.overlay = document.querySelector(options.overlay);
    this.open = this.open.bind(this);
    this.close = this.close.bind(this);
    this.overlay.addEventListener('click', this.close);
    this.createForm = this.createForm.bind(this);
  }

  open(content) {
    this.modal.innerHTML = content;
    this.modal.classList.add('open');
    this.overlay.classList.add('open');
  }

  close() {
    this.modal.classList.remove('open');
    this.overlay.classList.remove('open');
  }

  createForm(fighterInfo) {
    const divForm = document.createElement('div');
    const form = document.createElement('form');
    divForm.setAttribute('class', 'for-fighter-popup');
    form.setAttribute('class', 'fighter-popup');
    divForm.appendChild(form); // Create  Field for Name

    const fighterName = document.createElement('h2');
    fighterName.innerHTML = fighterInfo.name;
    form.appendChild(fighterName);
    const line = document.createElement('hr');
    form.appendChild(line);
    const linebreak = document.createElement('br');
    form.appendChild(linebreak); // Create  row for new Health

    const health = document.createElement('span');
    health.setAttribute('class', 'info');
    health.innerHTML = 'Health: ';
    form.appendChild(health);
    const healthValue = document.createElement('span');
    healthValue.setAttribute('class', 'value');
    healthValue.innerHTML = fighterInfo.health;
    form.appendChild(healthValue);
    const healthLabel = document.createElement('label');
    healthLabel.setAttribute('class', 'new-value');
    healthLabel.innerHTML = 'New Health: ';
    form.appendChild(healthLabel);
    const inputHealthElement = document.createElement('input');
    inputHealthElement.setAttribute('type', 'number');
    inputHealthElement.setAttribute('id', 'health');
    inputHealthElement.setAttribute('name', 'health');
    form.appendChild(inputHealthElement);
    form.appendChild(linebreak); // Create  row for new Attack

    const attack = document.createElement('span');
    attack.setAttribute('class', 'info');
    attack.innerHTML = 'Attack: ';
    form.appendChild(attack);
    const attackValue = document.createElement('span');
    attackValue.setAttribute('class', 'valueAttack');
    attackValue.innerHTML = fighterInfo.attack;
    form.appendChild(attackValue);
    const attackLabel = document.createElement('label');
    attackLabel.setAttribute('class', 'new-value');
    attackLabel.innerHTML = 'New Attack: ';
    form.appendChild(attackLabel);
    const inputAttackElement = document.createElement('input');
    inputAttackElement.setAttribute('type', 'number');
    inputAttackElement.setAttribute('id', 'attack');
    inputAttackElement.setAttribute('name', 'attack');
    form.appendChild(inputAttackElement);
    form.appendChild(linebreak); // Create  row for new Defense

    const defense = document.createElement('span');
    defense.setAttribute('class', 'info');
    defense.innerHTML = 'Defense: ';
    form.appendChild(defense);
    const defenseValue = document.createElement('span');
    defenseValue.setAttribute('class', 'value');
    defenseValue.innerHTML = fighterInfo.defense;
    form.appendChild(defenseValue);
    const defenseLabel = document.createElement('label');
    defenseLabel.setAttribute('class', 'new-value');
    defenseLabel.innerHTML = 'New Defense: ';
    form.appendChild(defenseLabel);
    const inputDefenseElement = document.createElement('input');
    inputDefenseElement.setAttribute('type', 'number');
    inputDefenseElement.setAttribute('id', 'defense');
    inputDefenseElement.setAttribute('name', 'defense');
    form.appendChild(inputDefenseElement);
    form.appendChild(linebreak); // Append checkbox

    const checkboxLabel = document.createElement('label');
    checkboxLabel.setAttribute('class', 'checkbox-text');
    checkboxLabel.innerHTML = 'Update Fighter ';
    form.appendChild(checkboxLabel);
    const inputCheckbox = document.createElement('input');
    inputCheckbox.setAttribute('type', 'checkbox');
    inputCheckbox.setAttribute('class', 'checkbox-icon');
    inputCheckbox.setAttribute('id', 'checkbox');
    form.appendChild(inputCheckbox); // Append Submit Button

    const submitelement = document.createElement('button');
    submitelement.setAttribute('type', 'button');
    submitelement.setAttribute('name', 'chooseFighter');
    submitelement.setAttribute('id', 'btnChoose');
    submitelement.innerText = 'Choose Fighter';
    form.appendChild(submitelement);
    return divForm;
  }

}

const popup = new Popup({
  modal: '.modal',
  overlay: '.overlay'
});

/***/ }),

/***/ "./src/javascript/services/fightersService.js":
/*!****************************************************!*\
  !*** ./src/javascript/services/fightersService.js ***!
  \****************************************************/
/*! exports provided: fighterService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fighterService", function() { return fighterService; });
/* harmony import */ var _helpers_apiHelper__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../helpers/apiHelper */ "./src/javascript/helpers/apiHelper.js");


class FighterService {
  async getFighters() {
    try {
      const endpoint = 'fighters.json';
      const apiResult = await Object(_helpers_apiHelper__WEBPACK_IMPORTED_MODULE_0__["callApi"])(endpoint, 'GET');
      return JSON.parse(atob(apiResult.content));
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(_id) {
    try {
      const endpoint = `details/fighter/${_id}.json`;
      const apiResult = await Object(_helpers_apiHelper__WEBPACK_IMPORTED_MODULE_0__["callApi"])(endpoint, 'GET');
      return JSON.parse(atob(apiResult.content));
    } catch (error) {
      throw error;
    }
  }

}

const fighterService = new FighterService();

/***/ }),

/***/ "./src/javascript/view.js":
/*!********************************!*\
  !*** ./src/javascript/view.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class View {
  constructor() {
    _defineProperty(this, "element", void 0);
  }

  createElement({
    tagName,
    className = '',
    attributes = {}
  }) {
    const element = document.createElement(tagName);
    element.classList.add(className);
    Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));
    return element;
  }

}

/* harmony default export */ __webpack_exports__["default"] = (View);

/***/ }),

/***/ "./src/styles/styles.css":
/*!*******************************!*\
  !*** ./src/styles/styles.css ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js!./styles.css */ "./node_modules/css-loader/dist/cjs.js!./src/styles/styles.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ })

/******/ });
//# sourceMappingURL=bundle.js.map